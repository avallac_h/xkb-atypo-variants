#!/usr/bin/env make

DESTDIR  ?= 
PREFIX   ?= /usr

BIN_DIR   = $(PREFIX)/bin
DATA_DIR  = $(PREFIX)/share/xkb-atypo-variants
DOCS_DIR  = $(PREFIX)/share/doc/xkb-atypo-variants
HOOKS_DIR = $(PREFIX)/share/libalpm/hooks

ABS_BIN_DIR   = $(DESTDIR)/$(BIN_DIR)
ABS_DATA_DIR  = $(DESTDIR)/$(DATA_DIR)
ABS_DOCS_DIR  = $(DESTDIR)/$(DOCS_DIR)
ABS_HOOKS_DIR = $(DESTDIR)/$(HOOKS_DIR)

VERSION=$(shell printf "%s+%s" "$$(git rev-list --count HEAD)" "$$(git rev-parse --short HEAD)")

install: install-patcher install-data install-docs install-hook

uninstall:
	rm -vf  "$(ABS_BIN_DIR)/patch-xkeyboard-config"
	rm -vf  "$(ABS_HOOKS_DIR)/xkb-atypo-variants-post-install.hook"
	rm -rvf "$(ABS_DATA_DIR)"
	rm -rvf "$(ABS_DOCS_DIR)"

clean:
	rm -rvf pkgs/archlinux/pkg
	rm -rvf pkgs/debian/pkg
	rm -vf  pkgs/archlinux/PKGBUILD.current
	rm -vf  pkgs/archlinux/xkb-atypo-variants*.pkg.*
	rm -vf  xkb-atypo-variants*.pkg.*
	rm -vf  xkb-atypo-variants*.deb

previews:
	$(foreach file, $(wildcard src/*.xml), ./generate-preview.sh $(file);)

install-patcher:
	install -v -Dm0755 patch-xkeyboard-config.py "$(ABS_BIN_DIR)/patch-xkeyboard-config"

install-data:
	install -v -dm0755 "$(ABS_DATA_DIR)"
	$(foreach file, $(wildcard src/*.symbols), install -v -t "$(ABS_DATA_DIR)" -m0644 $(file);)
	$(foreach file, $(wildcard src/*.xml), install -v -t "$(ABS_DATA_DIR)" -m0644 $(file);)

install-docs:
	install -v -dm0755 "$(ABS_DOCS_DIR)"
	$(foreach file, $(wildcard src/*.png), install -v -t "$(ABS_DOCS_DIR)" -m0644 $(file);)
	sed -e "s|](src/|](|" README.mkd > "$(ABS_DOCS_DIR)/README.mkd"

install-hook:
	install -v -dm0755 "$(ABS_HOOKS_DIR)"
	sed -e "s|@BIN_DIR@|$(BIN_DIR)|g" \
	    -e "s|@DATA_DIR@|$(DATA_DIR)|g" \
	    alpm/xkb-atypo-variants-post-install.hook.in \
	    > "$(ABS_HOOKS_DIR)/xkb-atypo-variants-post-install.hook"

arch-pkg: clean
	cp -v pkgs/archlinux/PKGBUILD pkgs/archlinux/PKGBUILD.current
	cd pkgs/archlinux && makepkg -e -p PKGBUILD.current
	mv -v pkgs/archlinux/xkb-atypo-variants*.pkg.* .

deb-pkg: clean
	install -v -dm0755 pkgs/debian/pkg
	install -v -Dm0755 pkgs/debian/DEBIAN/postinst  pkgs/debian/pkg/DEBIAN/postinst
	install -v -Dm0755 pkgs/debian/DEBIAN/prerm     pkgs/debian/pkg/DEBIAN/prerm
	install -v -Dm0644 pkgs/debian/DEBIAN/triggers  pkgs/debian/pkg/DEBIAN/triggers
	sed -e "s|@VERSION@|$(VERSION)|g" \
	    pkgs/debian/DEBIAN/control.in \
	    > pkgs/debian/pkg/DEBIAN/control
	make install DESTDIR=pkgs/debian/pkg
	rm -rv pkgs/debian/pkg/usr/share/libalpm
	dpkg-deb --build pkgs/debian/pkg .

.PHONY: install

