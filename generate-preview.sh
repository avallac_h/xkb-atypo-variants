#!/usr/bin/env bash
#
# Generate PNG previews for xkeyboard-config patches
#
# Dependencies: xmlstarlet, xdotool, imagemagick, pngquant, Inter font <https://rsms.me/inter/>
# References:
#  * <https://unix.stackexchange.com/questions/111624/how-to-display-the-current-keyboard-layout>
#  * <https://askubuntu.com/questions/787772/how-to-show-keyboard-chart-of-a-specific-layout-variant-from-command-line>
#
set -e
set -o pipefail

usage() {
    cat << EOF
$0: Generate PNG previews for xkeyboard-config patches
Usage:
  $0 INPUT.xml

EOF
}

xml_query() {
    local filename=$1
    local xpath=$2
    xmlstarlet fo -D "$filename" | xmlstarlet sel -t -v "$xpath"
}

xml_input=$1

[ -z "$xml_input" ] \
    && usage \
    && echo "$0: Missing input filename." \
    && exit 1

layout_name=$(xml_query  "$xml_input" "/xkbConfigRegistry/layoutList/layout/configItem/name")
variant_name=$(xml_query "$xml_input" "/xkbConfigRegistry/layoutList/layout/variantList/variant/configItem/name")
layout_desc=$(xml_query  "$xml_input" "/xkbConfigRegistry/layoutList/layout/configItem/description")
variant_desc=$(xml_query "$xml_input" "/xkbConfigRegistry/layoutList/layout/variantList/variant/configItem/description")
png_output="$(dirname "$xml_input")/${layout_name}_${variant_name}.png"

cat << EOF
Generating preview for:
     Input file: $xml_input
    Layout name: $layout_name
   Variant name: $variant_name
   Layout desc.: $layout_desc
  Variant desc.: $variant_desc
    Output file: $png_output
EOF

gkbd-keyboard-display -l "$layout_name"$'\t'"$variant_name" &
gkbd_pid="$!"
echo "gkbd-keyboard-display: PID=$gkbd_pid"
sleep 2

gkbd_wid=$(xdotool search --all --pid "$gkbd_pid" | sort | tail -n1)
echo "gkbd-keyboard-display: WID=$gkbd_wid"

[ -f "$png_output.tmp.png" ] && rm -v "$png_output.tmp.png"
import -window "$gkbd_wid" "$png_output.tmp.png"
kill "$gkbd_pid"

convert "$png_output.tmp.png" \
    -set page "%[fx:w*0.62]x%[fx:h*0.6]-%[fx:w*0.04]-%[fx:h*0.23]" -crop +0+0 \
    -trim +repage \
    -bordercolor '#F6F5F4' -border 4x4 \
    -background '#F6F5F4' -splice 0x5 \
    -font Inter-Bold -pointsize 18 \
    -background '#CCCCCC' \
    label:"${layout_name}:${variant_name} — ${layout_desc}: ${variant_desc}" \
    +swap -gravity Center -append \
    png:- | pngquant --force --speed 1 --strip --output "$png_output" -

rm -v "$png_output.tmp.png"

