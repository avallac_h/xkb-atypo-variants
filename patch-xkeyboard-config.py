#!/usr/bin/env python3
"""Install additional xkb variants by patching existing xkb rules and symbols

Copyright (C) 2019-2021 Andrew Kotsyuba <avallach2000@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import io
import re

from pathlib import Path
from typing import Dict

import lxml.etree as ET

XKB_DIR = "/usr/share/X11/xkb"
# XKB_DIR = "test"


class XkbVariantsPatcher:
    def __init__(self, xml_insertion_file: Path, sym_insertion_file: Path):
        # files
        self.xml_insertion_file: Path = xml_insertion_file
        self.sym_insertion_file: Path = sym_insertion_file

        # fmt: off
        # content
        self.xml_insertion = None    # ET.Element
        self.sym_insertion = ""      # str
        self.files: Dict[Path, io.StringIO] = {}

        # layout properties
        self.layout_name = ""        # for ex.: "us"
        self.layout_longdesc = ""    # for ex.: "English (US)"
        self.layout_shortdesc = ""   # for ex.: "en"

        # variant properties
        self.variant_name = ""       # for ex.: "dvorak"
        self.variant_longdesc = ""   # for ex.: "English (Dvorak)"
        self.variant_shortdesc = ""  # optional, for ex.: "en"
        # fmt: on

        self._parse_xml_insertion()
        self._parse_sym_insertion()
        self._check_mandatory_values()

    def __repr__(self):
        return (
            "%s(xml_insertion_file='%s', sym_insertion_file='%s', "
            + "layout_name='%s', layout_longdesc='%s', layout_shortdesc='%s', "
            + "variant_name='%s', variant_longdesc='%s', variant_shortdesc='%s', "
            + 'xml_insertion="%s", sym_insertion="%s", files=%s)'
        ) % (
            type(self).__name__,
            self.xml_insertion_file,
            self.sym_insertion_file,
            self.layout_name,
            self.layout_longdesc,
            self.layout_shortdesc,
            self.variant_name,
            self.variant_longdesc,
            self.variant_longdesc,
            type(self.xml_insertion),
            type(self.sym_insertion),
            len(self.files),
        )

    def _parse_xml_insertion(self):
        with self.xml_insertion_file.open() as fp:
            tree = ET.parse(fp)

        root = tree.getroot()

        # find layout/variant properties of the insertion in xml
        self.xml_insertion = root.find("./layoutList/layout/variantList/variant")

        lxpath = "./layoutList/layout/configItem"
        vxpath = "./layoutList/layout/variantList/variant/configItem"

        self.layout_name = root.findtext(lxpath + "/name")
        self.layout_longdesc = root.findtext(lxpath + "/description")
        self.layout_shortdesc = root.findtext(lxpath + "/shortDescription")

        self.variant_name = root.findtext(vxpath + "/name")
        self.variant_longdesc = root.findtext(vxpath + "/description")
        self.variant_shortdesc = root.findtext(vxpath + "/shortDescription")

        print(
            'Parsed rules patch data for variant "{}:{}" from "{}"'.format(
                self.layout_name, self.variant_name, self.xml_insertion_file
            )
        )

    def _parse_sym_insertion(self):
        with self.sym_insertion_file.open() as fp:
            self.sym_insertion = fp.read()
            print(
                'Loaded symbols patch data for variant "{}:{}" from "{}"'.format(
                    self.layout_name, self.variant_name, self.sym_insertion_file
                )
            )

    def _check_mandatory_values(self):
        # fmt: off
        if not self.layout_name:
            raise ValueError(
                '%s: Missing or empty layout "name".' % self.xml_insertion_file
            )
        if not self.layout_shortdesc:
            raise ValueError(
                '%s: Missing or empty layout "shortDescription".' % self.xml_insertion_file
            )
        if not self.variant_name:
            raise ValueError(
                '%s: Missing or empty variant "name".' % self.xml_insertion_file
            )
        if not self.variant_longdesc:
            raise ValueError(
                '%s: Missing or empty variant "Description".' % self.xml_insertion_file
            )
        if len(self.sym_insertion) == 0:
            raise ValueError(
                '%s: File is empty.' % self.sym_insertion_file
            )
        # fmt: on

    def patch_xml_rules(self, xkb_dir: Path, uninstall=False):
        filelist = [xkb_dir / "rules" / "base.xml", xkb_dir / "rules" / "evdev.xml"]

        if not (xkb_dir / "rules" / "xorg.xml").is_symlink():
            filelist.append(xkb_dir / "rules" / "xorg.xml")

        for filename in filelist:
            with filename.open() as fp:
                tree = ET.parse(fp)

            xkbConfigRegistry = tree.getroot()

            # Modify layout data
            for layout in xkbConfigRegistry.findall("./layoutList/layout"):

                # find layout to patch by "name" and "shortDescription" values
                if (
                    layout.findtext("./configItem/name") == self.layout_name
                    and layout.findtext("./configItem/shortDescription")
                    == self.layout_shortdesc
                ):
                    variantList = layout.find("./variantList")

                    # try to search and remove previously inserted data
                    for variant in variantList.findall("./variant"):
                        if variant.findtext("./configItem/name") == self.variant_name:
                            print(
                                (
                                    'Removing previously inserted "{}:{}" variant from '
                                    + '"{}"...'
                                ).format(self.layout_name, self.variant_name, filename)
                            )
                            variantList.remove(variant)

                    # inserting new data
                    if not uninstall:
                        if variantList is not None:
                            print(
                                'Inserting "{}:{}" variant to "{}"...'.format(
                                    self.layout_name, self.variant_name, filename
                                )
                            )
                            variantList.insert(len(variantList), self.xml_insertion)
                        else:
                            raise AssertionError(
                                (
                                    'Cannot find layout "{}" in "{}". This shouldn\'t happen.'
                                ).format(self.layout_name, filename)
                            )

            # Write output to virtual file
            vfile = io.StringIO()
            vfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            vfile.write('<!DOCTYPE xkbConfigRegistry SYSTEM "xkb.dtd">\n')

            # Serialize XML, fix "<variantList />" -> "<variantList/>", add a NL
            root = tree.getroot()
            s = ET.tostring(root, encoding="utf-8").decode("utf-8")
            vfile.write(s.replace("<variantList />", "<variantList/>"))
            vfile.write("\n")
            vfile.flush()
            vfile.seek(0)

            self.files[filename] = vfile

    def patch_lst_rules(self, xkb_dir: Path, uninstall=False):
        filelist = [xkb_dir / "rules" / "base.lst", xkb_dir / "rules" / "evdev.lst"]

        if not (xkb_dir / "rules" / "xorg.lst").is_symlink():
            filelist.append(xkb_dir / "rules" / "xorg.lst")

        # Define regexes
        re_variant_section_start = re.compile(r"^\s*!\s*variant")  # !variants
        re_variant_section_end = re.compile(r"^(?:\s*!\s*\w.*?|\s*)$")  # !smth or blank
        re_variant_record = re.compile(
            r"^\s*(?P<variant_name>\w[\w-]+)\s+(?P<layout_name>\w+):\s+.*?$"
        )

        for filename in filelist:
            with filename.open() as fp:
                content = fp.read().splitlines()

            # Indexes and flags for parsing
            inside_variant_section = False
            desired_layout_ends_here = None
            same_variant_indexes = []

            for idx, line in enumerate(content):

                # Sections are: "^!model", "^!layout", "^!variant" and "^!option", must
                # find the 3rd one
                if re.match(re_variant_section_start, line):
                    inside_variant_section = True
                elif inside_variant_section and re.match(re_variant_section_end, line):
                    inside_variant_section = False

                if inside_variant_section:
                    # Convert named re_variant_record groups to dict to perform further
                    # checks
                    record = [m.groupdict() for m in re_variant_record.finditer(line)]

                    # Find new insertion index (line no.), which can be the last line
                    # of desired layout or an index of previously insterted line.
                    # Saving both, decide later.
                    if record and record[0]["layout_name"] == self.layout_name:
                        desired_layout_ends_here = idx
                        if record[0]["variant_name"] == self.variant_name:
                            same_variant_indexes.append(idx)

            # Check for duplicate layout/variant records
            if len(same_variant_indexes) > 1:
                raise AssertionError(
                    (
                        'More than one occurance of "{}" variant for "{}" layout'
                        + 'found in "{}". Please reinstall xkeyboard-config package.'
                    ).format(self.variant_name, self.layout_name, filename)
                )

            # Is everything found?
            if desired_layout_ends_here is None and len(same_variant_indexes) == 0:
                raise AssertionError(
                    ('Cannot find layout "{}" in "{}". This shouldn\'t happen.').format(
                        self.layout_name, filename
                    )
                )

            # Construct a new line to modify content
            new_variant_line = "  {:16s}{}: {}".format(
                self.variant_name, self.layout_name, self.variant_longdesc
            )

            # Deciding what to do
            if same_variant_indexes:
                # remove line
                print(
                    'Removing previously inserted "{}:{}" variant from "{}"...'.format(
                        self.layout_name, self.variant_name, filename
                    )
                )
                del content[same_variant_indexes[0]]

                if not uninstall:
                    # insert line at the same index (replace line)
                    print(
                        'Inserting "{}:{}" variant to "{}"...'.format(
                            self.layout_name, self.variant_name, filename
                        )
                    )
                    content.insert(same_variant_indexes[0], new_variant_line)

            else:
                if not uninstall:
                    # insert line at the end of the desired layout
                    print(
                        'Inserting "{}:{}" variant to "{}"...'.format(
                            self.layout_name, self.variant_name, filename
                        )
                    )
                    content.insert(desired_layout_ends_here + 1, new_variant_line)

            # Write output to virtual file
            vfile = io.StringIO("\n".join(content) + "\n")
            vfile.flush()
            vfile.seek(0)

            self.files[filename] = vfile

    def patch_symbols(self, xkb_dir: Path, uninstall=False):
        filename = xkb_dir / "symbols" / self.layout_name

        with filename.open() as fp:
            content = fp.read().splitlines()

        # Define marks and indexes
        # fmt: off
        start_mark = (
            "// START OF THE PATCH-XKEYBOARD-CONFIG MANAGED BLOCK: %s:%s //"
            % (self.layout_name, self.variant_name)
        )
        end_mark = (
            "// END OF THE PATCH-XKEYBOARD-CONFIG MANAGED BLOCK: %s:%s //"
            % (self.layout_name, self.variant_name)
        )
        # fmt: on

        start_idx_list = []
        end_idx_list = []

        # Try to search previously installed block
        for idx, line in enumerate(content):
            if line.startswith(start_mark):
                start_idx_list.append(idx)
            if line.startswith(end_mark):
                end_idx_list.append(idx)

        # Check for duplicate variant blocks
        if len(start_idx_list) > 1 or len(end_idx_list) > 1:
            raise AssertionError(
                (
                    'More than one occurance of "{}" variant for "{}" layout'
                    + 'found in "{}". Please reinstall xkeyboard-config package.'
                ).format(self.variant_name, self.layout_name, filename)
            )

        # Modify content
        if start_idx_list:
            # remove block
            print(
                (
                    'Removing previously inserted "{}:{}" variant from ' + '"{}"...'
                ).format(self.layout_name, self.variant_name, filename)
            )
            content = content[: start_idx_list[0]] + content[end_idx_list[0] + 1 :]

            if not uninstall:
                # insert block at the same place
                print(
                    'Inserting "{}:{}" variant to "{}"...'.format(
                        self.layout_name, self.variant_name, filename
                    )
                )
                content = (
                    content[: start_idx_list[0]]
                    + [start_mark]
                    + [self.sym_insertion]
                    + [end_mark]
                    + content[end_idx_list[0] + 1 :]
                )

        else:
            if not uninstall:
                # insert block at the end of file
                print(
                    'Inserting "{}:{}" variant to "{}"...'.format(
                        self.layout_name, self.variant_name, filename
                    )
                )
                content = content + [start_mark] + [self.sym_insertion] + [end_mark]

        # Write output to virtual file
        vfile = io.StringIO("\n".join(content) + "\n")
        vfile.flush()
        vfile.seek(0)

        self.files[filename] = vfile

    def write_files_back(self):
        for filename, content in self.files.items():
            with filename.open("w") as fp:
                print("Writing file {}...".format(filename))
                fp.write(content.read())


def parse_args():
    parser = argparse.ArgumentParser(
        description="Install additional xkb variants by patching existing xkb rules "
        + "and symbols"
    )
    parser.add_argument(
        "-u",
        "--uninstall",
        help="uninstall specified previously applied patch",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-d",
        "--xkb-dir",
        help="directory contains xkb data (def.: {})".format(XKB_DIR),
        type=Path,
        default=Path(XKB_DIR),
    )
    parser.add_argument(
        "xml_insertion_files",
        metavar="custom_variant.xml",
        nargs="+",
        type=Path,
    )
    return parser.parse_args()


def main():
    args = parse_args()

    for xml_insertion_file in args.xml_insertion_files:
        sym_insertion_file = xml_insertion_file.with_suffix(".symbols")

        patcher = XkbVariantsPatcher(xml_insertion_file, sym_insertion_file)

        if args.uninstall:
            patcher.patch_xml_rules(args.xkb_dir, uninstall=True)
            patcher.patch_lst_rules(args.xkb_dir, uninstall=True)
            patcher.patch_symbols(args.xkb_dir, uninstall=True)
        else:
            patcher.patch_xml_rules(args.xkb_dir)
            patcher.patch_lst_rules(args.xkb_dir)
            patcher.patch_symbols(args.xkb_dir)

        patcher.write_files_back()


if __name__ == "__main__":
    main()
